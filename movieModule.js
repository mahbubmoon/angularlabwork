var myApp = angular.module('app',[]).filter('range', function(){
    return function(n) {
      var res = [];
      for (var i = 0; i < n; i++) {
        res.push(i);
      }
      return res;
    };
  });
myApp.controller("dynamicMenuController", function($scope, $http) {
$scope.movies= [];
$scope.movie = {
	'title' : '',
	'rank' : null,
	'id' : ''
}
$scope.skip = 0;
$scope.counter = [];
$scope.itemPerPage = 5;
$scope.lastPage = false;
$http.get('movies.json').success(function(data) { 
    //console.log("success!");
    $scope.movies = data;
	var checker = false;
	for(var i = 0; i < $scope.movies.length; i++){
	$scope.movies[i].edit = false;
	if(i%$scope.itemPerPage==0)
	$scope.counter.push(i/$scope.itemPerPage);
	}
        //console.log($scope.movies);
    });    
	$scope.addNewMovie = function(){
		$scope.movie ={
		'title' : '',
	'rank' : null,
	'id' : ''
		}
		$scope.movie.id = new Date();
		checker = false;
		$scope.counter = [];
		for(var i= 0; i< $scope.movies.length; i++){
		//console.log(i);
		if($scope.movies[i].rank == $scope.rank)
		{
			checker = true;
		}
		if(i%$scope.itemPerPage==0)
		$scope.counter.push(i/$scope.itemPerPage);
		if(i==$scope.movies.length-1 && (i+1)%$scope.itemPerPage==0){
		$scope.counter.push($scope.counter.length);
		console.log($scope.counter.length);
		}
		
		//if(i >= $scope.movie.rank){
		//	$scope.movies[i].rank = parseInt($scope.movies[i].rank) + 1;
		//}
		//console.log($scope.movie.rank);
		//$scope.movies[i].rank++;
		//console.log($scope.movies[i].title + ": "+ $scope.movies[i].rank);
		}
		console.log($scope.counter);
		if(checker){
		alert("Rank Exists!");
		}
		else{
		$scope.movie.title = $scope.title;
		$scope.movie.rank = $scope.rank;
		$scope.movies.push($scope.movie);
		alert("Added Successfully!");
		}
	}
	$scope.editEnable = function(d){
		for(var i = 0; i<$scope.movies.length;i++){
			if($scope.movies[i].id == d){
			if($scope.movies[i].edit == true)
				$scope.movies[i].edit = false;
				else
				$scope.movies[i].edit = true;
			}
		}
	}
	$scope.edit = function(d,e){
		for(var i = 0; i<$scope.movies.length;i++){
			if($scope.movies[i].id == d){
				if($scope.movies[i].title != e){
					$scope.movies[i].title = e;
				}
				else{
					alert("No changes to Update");
				}
				$scope.movies[i].edit = false;
			}
		}
	}
	$scope.delete = function(item) { 
		var index = $scope.movies.indexOf(item);
		$scope.movies.splice(index, 1);  
		$scope.counter = [];
		for(var i=0; i< $scope.movies.length; i++ ){
			if(i%$scope.itemPerPage==0)
			$scope.counter.push(i/$scope.itemPerPage );		
		}
	}
	
	$scope.updatePagination = function(d){
		$scope.counter = [];
		var cnt = 0;	
		for(var i=0; i< $scope.movies.length; i++ ){
			if($scope.movies[i].title.includes(d) || $scope.movies[i].rank.includes(d))
				if(i%$scope.itemPerPage==0){
					$scope.counter.push(cnt-0);
					cnt = cnt +1;
				}
		}
	}
	$scope.getNewData = function(d){
		$scope.skip = d*$scope.itemPerPage;
		console.log(d);
		for(var i=0; i< $scope.counter.length; i++){
			$("#"+i).removeClass("active");
		}
		$("#"+d).addClass("active");
		if($scope.skip + $scope.itemPerPage > $scope.movies.length-1){
			$scope.lastPage = true;
		}
		else{
			$scope.lastPage = false;
		}
	}
	
});